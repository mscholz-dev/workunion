import React from "react";
import Page from "../templates/layouts/Page";
import Container from "../templates/layouts/Container";
import CardImgWrapper from "../templates/components/Card/CardImgWrapper";
import CardIcon from "../templates/components/Card/CardIcon";
import Home from "../templates/components/Home/Home";
import IconPeople from "../public/icons/people.svg";
import IconBuilding from "../public/icons/building.svg";
import IconLetter from "../public/icons/letter.svg";
import IconCalculator from "../public/icons/calculator.svg";
import SelectContent from "../templates/components/SelectContent";
import { deskData } from "../templates/utils/deskData";

const Index = () => {
  const cardIconItems = [
    {
      id: 0,
      icon: <IconBuilding />,
      title: "Mise à disposition de bureaux",
      text: "Situé à deux pas de la gare Lille Flandres et de la Grand Place de Lille, offrez à votre entreprise une adresse exceptionnelle",
    },
    {
      id: 1,
      icon: <IconLetter />,
      title:
        "Domiciliation sociale et commerciale",
      text: "Offrez à votre entreprise une adresse exceptionnelle à laquelle faire parvenir votre courrier",
    },
    {
      id: 2,
      icon: <IconCalculator />,
      title:
        "Offre de gestion d'entreprise pour tiers",
      text: "Nous vous proposons de gérer toute la partie backoffice administrative et/ou commerciale des entreprises",
    },
    // {
    //   id: 3,
    //   icon: <IconPeople />,
    //   title: "Espace de coworking",
    //   text: "Situé à deux pas de la gare Lille Flandres et de la Grand Place de Lille, vous pourrez travailler ensemble sur des projets communs",
    // },
  ];

  //TODO: changer le text qui n'est pas à jour
  const cardImgText = `
  <p>WorkUnion est une société nouvelle qui développe un concept original de centre d'affaires associant la domiciliation d'entreprise, la mise à disposition de bureaux dans un cadre de partage d'expérience et avec un rapport coût/service lowcost. WorkUnion a pour objectif d'accompagner l'entreprise à sa création et durant les différentes étapes de son développement.</p>

  <div class="card-img-content-position">

    <div>
      <h3>Un emplacement au cœur de Lille :</h3>
  <ul>
    <li>16-18 rue Faidherbe - 59000 LILLE</li>
    <li>250 mètres de la gare de Lille Flandres, du métro et des bus</li>
    <li>600 mètres de la gare de Lille Europe</li>
    <li>30 minutes approximatives de tous les centres économiques de l'Agglomération lilloise</li>
  </ul>
  </div>
  
<div>
    <h3>400 M² de bureaux comprenant :</h3>

    <ul>
      <li>8 bureaux de 5 à 20 m²</li>
      <li>2 salles de réunion</li>
      <li>Un open space de coworking</li>
      <li>Une salle de visio conférence toute équipée</li>
      <li>Un équipement internet fibre</li>
      <li>Boissons fraîches et machine à café à votre disposition</li>
    </ul>
</div>
<div>
    <h3>Une équipe à votre disposition comprenant :</h3>

    <ul>
      <li>Un manager de centre</li>
      <li>Un(e) assistant(e) Web Marketing pouvant vous accompagner dans toutes vos approches informatiques et web</li>
    </ul>
</div>
  </div>
  `;

  const selectContentItems = [
    {
      id: 0,
      title: "Domiciliation simple",
      price: "47",
      content: `
        <ul>
          <li>Une adresse en plein centre de Lille (rue Faidherbe)</li>
          <li>Une boite aux lettres</li>
          <li>La mise à disposition du courrier</li>
        </ul>
      `,
    },
    {
      id: 1,
      title: "Domiciliation avec réexpédition",
      price: "55",
      content: `
        <ul>
          <li>Une adresse en plein centre de Lille (rue Faidherbe)</li>
          <li>Une boite aux lettres</li>
          <li>La réexpédition hebdomadaire du courrier par voie postale</li>
        </ul>
      `,
    },
    {
      id: 2,
      title: "Domiciliation avec retranscription",
      price: "55",
      content: `
        <ul>
          <li>Une adresse en plein centre de Lille (rue Faidherbe)</li>
          <li>Une boite aux lettres</li>
          <li>La retranscription par mail à réception du courrier</li>
        </ul>
      `,
    },
    {
      id: 3,
      title: "Option",
      price: "5.99",
      content: `
        <p>
          Pour 5.99€ HT/mois, en complément d'une des formules de domiciliation présentées ci-dessus, obtenez 1/2 journée de mise à disposition d'un bureau équipé spécialisé (sur prise de rendez-vous).
        </p>
      `,
    },
  ];

  return (
    <Page
      title="Accueil"
      description="Workunion propose aux entreprises des services de domiciliations ainsi que la mise à disposition de bureaux!"
    >
      <Home />
      <section className="card-icon-section">
        <Container className="card-icon-wrapper">
          {cardIconItems.map(
            ({ id, icon, title, text }) => (
              <CardIcon
                key={id}
                icon={icon}
                title={title}
                text={text}
              />
            ),
          )}
        </Container>
      </section>

      <CardImgWrapper
        title="Quelle est notre vision chez WorkUnion ?"
        text={cardImgText}
        subtitle="Les moyens que WorkUnion met à la disposition de ses adhérents :"
        items={deskData}
      />

      <SelectContent
        title="Nos tarifs de domiciliations"
        items={selectContentItems}
      />
    </Page>
  );
};

export default Index;
