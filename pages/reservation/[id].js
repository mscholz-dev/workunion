import React, {
  useEffect,
  useState,
} from "react";
import Page from "../../templates/layouts/Page";
import {
  Calendar,
  momentLocalizer,
} from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import moment from "moment";
import "moment/locale/fr";
import IconChevronLeft from "../../public/icons/chevron-left.svg";
import IconChevronRight from "../../public/icons/chevron-right.svg";
import Container from "../../templates/layouts/Container";
import Section from "../../templates/layouts/Section";
import FormCalendar from "../../templates/components/Form/FormCalendar";
import DeskBookingApi from "../api/deskBooking";

// Fin des imports

// Composant ReactBigCalendar
const ReactBigCalendar = ({ id, bookings }) => {
  // Déclaration de variables

  const [currentSlot, setCurrentSlot] = useState(
    {},
  );

  const [slot, setSlot] = useState([]);

  const subject = "bureau actuel";

  const mensuel = true;

  const localizer = momentLocalizer(moment);

  // Déclaration des fonctions

  useEffect(() => {
    for (const { start, end } of bookings) {
      slot.push({
        title: "Réserver",
        start: new Date(start),
        end: new Date(end),
      });
    }
  }, [bookings]);

  const messagesOptions = {
    // Modification du texte de react big calendar de base
    date: "Date",
    time: "",
    event: "évènement",
    allDay: "Tous les jours",
    week: "Semaine",
    work_week: "Semaine de travail",
    day: "Jour",
    month: "Mois",
    previous: (
      <span className="calendar-chevron">
        <IconChevronLeft />
      </span>
    ),
    next: (
      <span className="calendar-chevron">
        <IconChevronRight />
      </span>
    ),
    yesterday: "Hier",
    tomorrow: "Demain",
    today: "Aujourd'hui",
    agenda: "Agenda",

    noEventsInRange:
      "There are no events in this range.",

    showMore: (total) => `+${total} plus`,
  };

  const handleSelectable = (slotInfo) => {
    // Empêche une réservation d'être avant 9h et après 18h
    // change 0h to 9h
    // console.log(slotInfo.start, slotInfo.end);
    if (
      slotInfo.start.getHours() >= 0 &&
      slotInfo.start.getHours() <= 9
    )
      slotInfo.start.setHours(9);

    // change 0h to 18h
    if (
      slotInfo.end.getHours() >= 19 ||
      slotInfo.end.getHours() === 0
    ) {
      slotInfo.end.setDate(
        slotInfo.end.getDate() - 1,
      );
      slotInfo.end.setHours(18);
    }

    return slotInfo;
  };

  const handleViews = (monthly) => {
    // Grâce à une props, passe du coworking (vue à la semaine de travail) à la vue au mois
    if (monthly) {
      return ["month"];
    } else if (!monthly) {
      return ["work_week"];
    }
  };

  const handleRemoveEvent = () => {
    // Clear l'event actuel
    currentSlot.start = null;
    currentSlot.end = null;
  };

  const handleSelectSlot = (slotInfo) => {
    // Permet de customiser la plage voulu
    const nextDay = new Date();
    nextDay.setDate(nextDay.getDate() + 1);
    nextDay.setHours(23);
    let reservation = {};

    if (slotInfo.start > nextDay) {
      handleSelectable(slotInfo);
      if (
        slotInfo.action === "click" ||
        slotInfo.action === "select"
      ) {
        if (
          currentSlot.start &&
          currentSlot.end
        ) {
          if (
            new Date(slotInfo.start) <
            new Date(currentSlot.start)
          ) {
            currentSlot.start = slotInfo.start;
          } else if (
            new Date(
              currentSlot.start,
            ).getTime() ===
            new Date(slotInfo.start).getTime()
          ) {
            currentSlot.start = null;
            currentSlot.end = null;
          } else {
            currentSlot.end = slotInfo.end;
          }
        } else {
          currentSlot.start = slotInfo.start;
          currentSlot.end = slotInfo.end;
        }
      } else {
        currentSlot.start = slotInfo.start;
        currentSlot.end = slotInfo.end;
      }
    }

    reservation = {
      title: "Réservation en cours",
      start: currentSlot.start,
      end: currentSlot.end,
    };

    setCurrentSlot(reservation);
  };

  const calendarOptions = {
    // Les différentes options nécessaire pour le calendrier
    localizer: localizer,
    events: [currentSlot],
    startAccessor: "start",
    endAccessor: "end",
    locale: "fr",
    views: handleViews(mensuel), // ["month", "work_week"]
    defaultView: handleViews(mensuel),
    onSelectSlot: handleSelectSlot,
    selectable: handleSelectable,
    onSelectEvent: handleRemoveEvent,
    longPressThreshold: 0,
    min: moment("9:00am", "h:mma").toDate(),
    max: moment("18:00pm", "h:mma").toDate(),
    messages: messagesOptions,
    step: 30,
    timeslots: 2,
  };

  // Retour de la fonction principal

  return (
    <Page
      title="React Big Calendar"
      description="Workunion propose aux entreprises des services de domiciliations ainsi que la mise à disposition de bureaux!"
      padding
    >
      {/* toogle pour reservation mensuel / coworking */}

      <Section>
        <Container>
          <div className="calendar">
            <Calendar {...calendarOptions} />
          </div>
          <span className="calendar-separator"></span>
          <FormCalendar initSubject={subject} />
        </Container>
      </Section>

      {/* form avec data Nom email téléphone (société) nombres de personnes */}
    </Page>
  );
};

export default ReactBigCalendar;

export const getServerSideProps = async (ctx) => {
  const bookings = await DeskBookingApi.get(
    ctx.params.id,
  );

  return {
    props: {
      bookings: bookings.data.bookings || [],
    },
  };
};
