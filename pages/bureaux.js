import React from "react";
import Page from "../templates/layouts/Page";
import Desk from "../templates/components/DeskPresentation/Desk";
import Container from "../templates/layouts/Container";
import { deskData } from "../templates/utils/deskData";

const Bureaux = () => {
  return (
    <Page
      title="Bureaux"
      description="Workunion propose aux entreprises des services de domiciliations ainsi que la mise à disposition de bureaux!"
    >
      <div className="desk-top">
        <Container>
          <h1>
            Les bureaux mis à votre disposition
          </h1>
          <p>
            En plein centre de Lille, entre la
            gare Lille Flandres et la Grande
            Place, découvrez nos bureaux spacieux
            et lumineux afin d'ouvrir votre esprit
            à la créativité et à la productivité.
            Ces différents locaux, inspirent
            différentes philosophies, c'est pour
            cela qu'elles portent le nom de
            certains des plus grands philosophes.
          </p>
        </Container>
      </div>
      <Container>
        <div className="desk-cards">
          {deskData.map(
            ({
              id,
              imgSlider,
              title,
              text,
              priceM,
              surface,
              description,
              availability,
            }) => (
              <Desk
                key={id}
                id={id}
                imgSlider={imgSlider}
                title={title}
                text={text}
                priceM={priceM}
                surface={surface}
                description={description}
                availability={availability}
              />
            ),
          )}
        </div>
      </Container>
    </Page>
  );
};

export default Bureaux;
