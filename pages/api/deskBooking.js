import api from "./index";

export default class DeskBookingApi {
  static async create(data) {
    return await api
      .post(`/desk/booking`, data, {
        headers: {
          "Content-Type":
            "application/x-www-form-urlencoded",
          accept: "application/json",
        },
      })
      .then((res) => res);
  }

  static async get(id) {
    return await api
      .get(`/desk/booking/${id}`, {
        headers: {
          accept: "application/json",
        },
      })
      .then((res) => res);
  }
}
