import React, {
  useEffect,
  useState,
} from "react";
import FormContact from "../templates/components/Form/FormContact";
import Page from "../templates/layouts/Page";

const Contact = () => {
  const [subject, setSubject] = useState("");

  const handleVerifyData = (data) => {
    if (!data) return;
    if (data.title) {
      setSubject(`${data.title}`);
      return true;
    }

    return false;
  };

  const handleSubject = () => {
    const data = JSON.parse(
      window.localStorage.getItem("prediction"),
    );
    handleVerifyData(data);
    window.localStorage.removeItem("prediction");
  };

  useEffect(() => {
    handleSubject();
  }, []);

  return (
    <Page
      title="Contact"
      noFooter
      padding
      description="Workunion propose aux entreprises des services de domiciliations ainsi que la mise à disposition de bureaux!"
    >
      <FormContact initSubject={subject} />
    </Page>
  );
};

export default Contact;
