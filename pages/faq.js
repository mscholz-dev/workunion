import React from "react";
import Page from "../templates/layouts/Page";
import FaqItem from "../templates/components/FaqItem";
import { faqData } from "../templates/utils/faqData";

const Faq = () => {
  return (
    <Page
      title="FAQ"
      padding
      description="Workunion propose aux entreprises des services de domiciliations ainsi que la mise à disposition de bureaux!"
      className="faq-bg"
      footerLight
    >
      <section className="faq-section">
        <h1 className="faq-title">
          Foire aux questions
        </h1>

        {faqData.map(({ id, title, content }) => (
          <FaqItem
            key={id}
            title={title}
            content={content}
          />
        ))}
      </section>
    </Page>
  );
};

export default Faq;
