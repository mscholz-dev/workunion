const altPreset = "Présentation du bureau";

export const deskData = [
  {
    id: 0,
    imgSrc: "/desk/marx/one.webp",
    imgAlt: `${altPreset} Marx`,
    imgSlider: [
      {
        id: 0,
        imgSrc: "/desk/marx/one.webp",
        imgAlt: `${altPreset} Marx`,
      },
      {
        id: 1,
        imgSrc: "/desk/marx/two.webp",
        imgAlt: `${altPreset} Marx vue sous un autre angle`,
      },
      {
        id: 2,
        imgSrc: "/desk/marx/three.webp",
        imgAlt: `${altPreset} Marx vue sous un nouvel angle`,
      },
    ],
    title: "Marx",
    description:
      "Si vous êtes aliéné à votre travail sans y perdre votre créativité, ce bureau sera votre exutoire.",
    text: "Ce bureau se compose d'une grande pièce entièrement personnalisable afin de vous y sentir comme chez vous ainsi qu'un local pour rester constamment organiser.",
    surface: 20,
    priceM: {
      HT: 550,
      expenses: 50,
    },
    availability: {
      now: true,
      day: "",
      month: "",
    },
  },
  {
    id: 1,
    imgSrc: "/desk/epictete/one.webp",
    imgAlt: `${altPreset} Epictète`,
    imgSlider: [
      {
        id: 0,
        imgSrc: "/desk/epictete/one.webp",
        imgAlt: `${altPreset} Epictète`,
      },
      {
        id: 1,
        imgSrc: "/desk/epictete/two.webp",
        imgAlt: `${altPreset} Epictète vue sous un autre angle`,
      },
    ],
    title: "Epictète",
    description:
      "Si vous aspirez à atteindre l'ataraxie, ce bureau donnera lieu à votre tranquillité d'esprit et à votre sérénité.",
    text: "Ce bureau se compose d'une grande pièce chaleureuse et authentique entièrement personnalisable afin de vous y sentir comme chez vous ainsi qu'une armoire pour vous organiser comme bon vous semble.",
    surface: 12,
    priceM: {
      HT: 500,
      expenses: 50,
    },
    availability: {
      now: true,
      day: "",
      month: "",
    },
  },
  {
    id: 2,
    imgSrc: "/desk/freud/one.webp",
    imgAlt: `${altPreset} Freud`,
    imgSlider: [
      {
        id: 0,
        imgSrc: "/desk/freud/one.webp",
        imgAlt: `${altPreset} Freud`,
      },
      {
        id: 1,
        imgSrc: "/desk/freud/two.webp",
        imgAlt: `${altPreset} Freud vue sous un autre angle`,
      },
    ],
    title: "Freud",
    description:
      "Si vous souhaitez libérer vos idées créatives inconscientes, ce bureau vous permettra d'accéder à vos pensées les plus enfouies.",
    text: "Ce bureau se compose d'une pièce chaleureuse entièrement personnalisable afin de vous y sentir comme chez vous.",
    surface: 5,
    priceM: {
      HT: 250,
      expenses: 25,
    },
    availability: {
      now: true,
      day: "",
      month: "",
    },
  },
  {
    id: 3,
    imgSrc: "/desk/platon/one.webp",
    imgAlt: `${altPreset} Platon`,
    imgSlider: [
      {
        id: 0,
        imgSrc: "/desk/platon/one.webp",
        imgAlt: `${altPreset} Platon`,
      },
      {
        id: 1,
        imgSrc: "/desk/platon/two.webp",
        imgAlt: `${altPreset} Platon vue sous un autre angle`,
      },
    ],
    title: "Platon",
    description:
      "Si vous êtes intéressé par la recherche de vérité, ce bureau aura les moyens vous y aider.",
    text: "Ce bureau se compose d'une grande pièce chaleureuse et rustique entièrement personnalisable afin de vous y sentir comme chez vous.",
    surface: 20,
    priceM: {
      HT: 530,
      expenses: 50,
    },
    availability: {
      now: true,
      day: "",
      month: "",
    },
  },
  {
    id: 4,
    imgSrc: "/desk/nietzsche/one.webp",
    imgAlt: `${altPreset} Nietzsche`,
    imgSlider: [
      {
        id: 0,
        imgSrc: "/desk/nietzsche/one.webp",
        imgAlt: `${altPreset} Nietzsche`,
      },
      {
        id: 1,
        imgSrc: "/desk/nietzsche/two.webp",
        imgAlt: `${altPreset} Nietzsche vue sous un autre angle`,
      },
      {
        id: 1,
        imgSrc: "/desk/nietzsche/three.webp",
        imgAlt: `${altPreset} Nietzsche vue sous un nouvel angle`,
      },
    ],
    title: "Nietzsche",
    description:
      "Si vous cherchez à vous libérer de vos croyances pour pouvoir vivre pleinement, ce bureau sera fait pour vous.",
    text: "Ce bureau se compose d'une pièce unique entièrement personnalisable afin de vous y sentir comme chez vous.",
    surface: 10,
    priceM: {
      HT: 450,
      expenses: 35,
    },
    availability: {
      now: true,
      day: "",
      month: "",
    },
  },
  {
    id: 5,
    imgSrc: "/desk/rousseau/one.webp",
    imgAlt: `${altPreset} Rousseau`,
    imgSlider: [
      {
        id: 0,
        imgSrc: "/desk/rousseau/one.webp",
        imgAlt: `${altPreset} Rousseau`,
      },
      {
        id: 1,
        imgSrc: "/desk/rousseau/two.webp",
        imgAlt: `${altPreset} Rousseau vue sous un autre angle`,
      },
    ],
    title: "Rousseau",
    description:
      "Si vous êtes quelqu'un qui soutient que l'homme est bon par nature, ce bureau vous conviendra à merveilles.",
    text: "Ce bureau se compose d'une grande pièce entièrement personnalisable afin de vous y sentir comme chez vous ainsi qu'un local pour rester constamment organiser.",
    surface: 12,
    priceM: {
      HT: 530,
      expenses: 50,
    },
    availability: {
      now: true,
      day: "",
      month: "",
    },
  },
];
