export const faqData = [
  {
    id: 0,
    title:
      "Quels sont les avantages de la domiciliation commerciale ?",
    content: `
      <ul>
        <li>En premier lieu, cette solution offre une flexibilité indéniable, sans parler du moindre coût ;</li>
        <li>La domiciliation commerciale permet, aussi, de profiter d'une adresse administrative ;</li>
        <li>Par ailleurs, elle préserve la vie privée de l'exécutif ;</li>
        <li>Les services que nous proposons, au sein de WorkUnion, permettent à l'entrepreneur de gagner du temps sur certaines tâches ;</li>
      </ul>
    `,
  },
  {
    id: 1,
    title:
      "Quelles sont les pièces justificatives requises pour la signature du contrat ?",
    content: `
      <p>Il faut se munir d'une attestation de domiciliation comportant :</p>

      <ul>
        <li>Coordonnées du représentant légal de l'entreprise (nom, prénom et adresse) ;</li>
        <li>Adresse précise du siège social telle qu'elle apparaîtra sur l'extrait Kbis ;</li>
        <li>Dénomination sociale de la société ;</li>
        <li>Numéro unique d'identification ou SIREN ;</li>
      </ul>

      <p>L'attestation doit être daté et signé par le dirigeant et transmis au CFE accompagné d'un :</p>

      <ul>
        <li>Justificatif de domicile de moins de 3 mois ;</li>
        <li>Dossier d'immatriculation ou de changement d'adresse ;</li>
      </ul>
      `,
  },
  {
    id: 2,
    title:
      "Quels sont les différents moyens de paiement ?",
    content: `
      <ul>
        <li>Par chèque ;</li>
        <li>Par virement ;</li>
        <li>En espèce ;</li>
      </ul>
    `,
  },
  {
    id: 3,
    title: "Quels sont les services proposés ?",
    content: `
      <p>Nous offrons une mise à disposition du courrier avec boîte aux lettres (quel voie?), une équipe comprenant un manager de centre et un(e) assistant(e) Web Marketing.</p>
    `,
  },
  {
    id: 4,
    title:
      "Comment résilier le contrat de la domiciliation ?",
    content: `
      <p>L'entreprise domiciliée a la possibilité d'arrêter le contrat à tout moment en respectant le préavis prévu. Elle s'opère par lettre recommandée avec accusé de réception.</p>

      <p>Cependant, la rupture du contrat, si l'entreprise n'est pas motivée par la fermeture de l'entreprise, amène à un transfert de l'entreprise, les formalités seront nécessaire puisque le changement entraîne une modification du statut.</p>

      <p>Pour finir, une preuve de transfert de siège ou d'annulation, délivrée par le greffe du tribunal de commerce, doit être adressée à la société de domiciliation.</p>
    `,
  },
  {
    id: 5,
    title:
      "Comment se déroule une demande de domiciliation ?",
    content: `
      <p>Tout d'abord, il faut se munir d'un document Cerfa de demande de domiciliation (cela peut se faire par voie électronique par l'envoie d'un formulaire Cerfa scanné ou d'un envoie d'informations visant à une prise de rendez-vous.</p>

      <p>La structure domiciliataire doit accuser réception de la demande et soumettre un entretien au demandeur. Notre organisme dispose de deux mois à partir de la date de dépôt de la demande pour y répondre, au-delà la demande sera rejetée.</p>
    `,
  },
  {
    id: 6,
    title:
      "Qui est éligible à la domiciliation ?",
    content: `
      <p>Toutes les entreprises sont concernées par la domiciliation.</p>
    `,
  },
  {
    id: 7,
    title:
      "Combien coûte le transfert du siège social ?",
    content: `
      <p>Le transfère d'une société (SAS, EURL, SARL ...) coûte environ 250€ pour les sociétés à associé unique et 350€ pour les sociétés à plusieurs associés.</p>

      <p>Le coût s'alourdit si la société change de ressort de greffe de tribunal de commerce, 350 à 450€</p>

      <p>De plus, le prix dépend de plusieurs paramètres : du statut juridique de votre entreprise, du lieu choisi et de l'accessibilité de l'adresse.</p>
    `,
  },
];
