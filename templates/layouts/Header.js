import React, {
  useState,
  useEffect,
  useRef,
} from "react";
import Link from "next/link";

const Header = ({ myRef, light }) => {
  const listRef = useRef(null);
  const listContainerRef = useRef(null);

  const [pathname, setPathname] = useState("");
  const [open, setOpen] = useState(false);

  const headerData = [
    {
      id: 0,
      title: "Accueil",
      url: "/",
    },
    {
      id: 1,
      title: "Bureaux",
      url: "/bureaux",
    },
    {
      id: 2,
      title: "FAQ",
      url: "/faq",
    },
  ];

  const handleHeaderSize = (isOpen = null) => {
    if (
      !myRef.current ||
      !listRef.current ||
      !listContainerRef.current
    )
      return;

    if (window.innerWidth > 768) {
      setOpen(false);
      myRef.current.style.height = "56px";
      listRef.current.style.height = "56px";
      listContainerRef.current.style.height =
        "56px";
      return;
    }

    if (isOpen === null) {
      if (
        myRef.current.style.height === "56px" &&
        listRef.current.style.height === "56px" &&
        listContainerRef.current.style.height ===
          "56px"
      ) {
        myRef.current.style.height = "56px";
        listRef.current.style.height = "0px";
        listContainerRef.current.style.height =
          "0px";
        return;
      }

      return;
    }

    if (!isOpen) {
      myRef.current.style.height = "56px";
      listRef.current.style.height = "0px";
      listContainerRef.current.style.height =
        "0px";
      return;
    }

    let listHeight = 10;

    for (const child of listContainerRef.current
      .children) {
      listHeight += child.offsetHeight;
    }

    myRef.current.style.height = `calc(56px + ${listHeight}px + 12px)`;
    listRef.current.style.height = `${listHeight}px`;
    listContainerRef.current.style.height = `${listHeight}px`;
  };

  const handleBurger = () => {
    setOpen(!open);
    handleHeaderSize(!open);
  };

  useEffect(() => {
    setPathname(window.location.pathname);
    window.addEventListener("resize", () =>
      handleHeaderSize(),
    );
  }, []);

  return (
    <header
      ref={myRef}
      className={`header${
        open ? " header-open" : ""
      }`}
      aria-label="En-tête de page."
    >
      <div className="header-container">
        <div className="header-brand-wrapper">
          <Link href="/" className="header-brand">
            <img
              src="/img/workunion.webp"
              alt="Logo de Workunion"
              className="header-brand-logo"
            />
            <span className="header-brand-label">
              Workunion
            </span>
          </Link>

          <div
            className="header-burger"
            onClick={handleBurger}
            tabIndex={0}
            onKeyDown={handleBurger}
          >
            <span />
            <span />
          </div>
        </div>

        <div
          ref={listRef}
          className="header-list"
        >
          <div
            ref={listContainerRef}
            className="header-list-container"
          >
            {headerData.map(
              ({ id, title, url }) => (
                <Link
                  key={id}
                  href={url}
                  className={`header-list-item link${
                    pathname === url
                      ? " header-list-item-active"
                      : ""
                  }`}
                >
                  {title}
                </Link>
              ),
            )}

            <div className="btn-header-pt">
              <a
                href="/contact"
                className="btn-header"
              >
                CONTACT
              </a>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
