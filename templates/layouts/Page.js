import React from "react";
import { useRef } from "react";
import Head from "next/head";
import Header from "./Header";
import Footer from "./Footer";
import { ToastContainer } from "react-toastify";

const Page = ({
  title,
  children,
  padding,
  noFooter,
  className,
  footer,
  description,
  footerLight,
}) => {
  const headerRef = useRef(null);
  const mainRef = useRef(null);
  const footerRef = useRef(null);

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0"
        />
        <meta name="theme-color" content="#fff" />
        <meta name="geo.region" content="FR-59" />
        <meta
          name="geo.placename"
          content="Lille"
        />
        <meta
          name="geo.position"
          content="50.636565;3.063528"
        />
        <meta
          name="ICBM"
          content="50.636565, 3.063528"
        />

        <title>{`WORKUNION • ${title}`}</title>

        <meta
          name="description"
          content={description}
        />

        <meta
          name="robots"
          content="index, follow"
        />

        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div
        className={`body-wrapper${
          className ? ` ${className}` : ""
        }`}
      >
        <Header myRef={headerRef} />
        <main
          ref={mainRef}
          className={`main${
            padding ? " main-padding" : ""
          }${
            footer ? " footer-top-content" : ""
          }`}
        >
          {children}
        </main>
        {!noFooter && (
          <Footer
            myRef={footerRef}
            light={footerLight}
          />
        )}
        <ToastContainer />
      </div>
    </>
  );
};

export default Page;
