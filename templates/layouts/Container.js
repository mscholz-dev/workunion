import React from "react";

const Container = ({ children, className }) => {
  return (
    <div
      className={`container-main${
        className ? ` ${className}` : ""
      }`}
    >
      {children}
    </div>
  );
};

export default Container;
