import React from "react";

const Edito = ({ children }) => {
  return <div className="edito">{children}</div>;
};

export default Edito;
