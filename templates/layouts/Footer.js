import React from "react";
import Facebook from "../../public/icons/facebook.svg";
import Instagram from "../../public/icons/instagram.svg";
import Linkedin from "../../public/icons/linkedin.svg";
import Mail from "../../public/icons/email.svg";
import Location from "../../public/icons/location.svg";
import Phone from "../../public/icons/phone.svg";
import Mobile from "../../public/icons/mobile.svg";

const Footer = ({ myRef, light }) => {
  return (
    <footer
      ref={myRef}
      className={`footer${
        light ? " footer-light" : ""
      }`}
      aria-label="Pied de page comportant les informations de contact de WorkUnion et un accès aux réseaux sociaux"
    >
      <span className="footer-separator"></span>
      <div className="footer-container">
        <div className="footer-contact">
          <h5>Adresse de WorkUnion</h5>
          <span></span>
          <a
            href="https://www.google.com/maps/place/16+Rue+Faidherbe,+59800+Lille/@50.6368875,3.0639326,17z/data=!3m1!4b1!4m5!3m4!1s0x47c2d589ab427ce3:0x4e57745c5f7bf46a!8m2!3d50.6368841!4d3.0661213?hl=fr"
            target="_blank"
          >
            <address className="footer-location">
              <Location className="footer-icons" />
              16-18 Rue Faidherbe, Lille 59000
            </address>
          </a>
        </div>
        <div className="footer-contact">
          <h5>Nos coordonnées :</h5>
          <span></span>
          <p>
            <Mobile className="footer-icons" /> 07
            83 200 600
          </p>
          <a href="mailto:direction.workunion@gmail.com">
            <Mail className="footer-icons" />
            contact.workunion@gmail.com
          </a>
          <ul className="footer-media">
            <li>
              <a
                title="Linkedin WorkUnion"
                target="_blank"
                href="https://www.linkedin.com/in/workunion-lille-776b1a255"
              >
                <Linkedin className="btn-media" />
              </a>
            </li>
            <li>
              <a
                href="https://www.instagram.com/workunionlille/?hl=fr"
                target="_blank"
                title="Instagram"
              >
                <Instagram className="btn-media" />
              </a>
            </li>
            <li>
              <a
                href="https://www.facebook.com/profile.php?id=100088556820443"
                target="_blank"
                title="Facebook"
              >
                <Facebook className="btn-media" />
              </a>
            </li>
          </ul>
        </div>
      </div>

      <p className="footer-reserved">
        Copyright WorkUnion©2022. Tous droits
        réservés.
      </p>
    </footer>
  );
};

export default Footer;
