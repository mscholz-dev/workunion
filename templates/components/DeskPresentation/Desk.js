import React, { useRef } from "react";
import Slider from "react-slick";
import Link from "next/link";
import ChevronR from "../../../public/icons/chevron-right.svg";
import ChevronL from "../../../public/icons/chevron-left.svg";

const Desk = ({
  id,
  imgSlider,
  title,
  text,
  priceM,
  surface,
  description,
  availability,
}) => {
  const sliderRef = useRef(null);
  const settings = {
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: false,
    dots: true,
  };

  const next = () => {
    if (!sliderRef.current) return;
    sliderRef.current.slickNext();
  };

  const prev = () => {
    if (!sliderRef.current) return;
    sliderRef.current.slickPrev();
  };

  const handleRedirect = (title) => {
    window.localStorage.setItem(
      "prediction",
      JSON.stringify({
        title,
      }),
    );
  };

  return (
    <div id={`${id}`} className="desk-container">
      <div className="desk-slider-container">
        <Slider ref={sliderRef} {...settings}>
          {imgSlider.map(
            ({ id, imgSrc, imgAlt }) => (
              <img
                key={id}
                className="desk-img"
                src={`/img/${imgSrc}`}
                alt={`/img/${imgAlt}`}
              />
            ),
          )}
        </Slider>
        <button
          className="desk-sliderArrow"
          onClick={prev}
        >
          <ChevronL />
        </button>
        <button
          className="desk-sliderArrow"
          onClick={next}
        >
          <ChevronR />
        </button>
      </div>
      <div>
        <div className="desk-title">
          <h3>Bureau {title}</h3>
          <p>Surface : ≈ {surface} m²</p>
          <p>Tarif/Mois : {priceM.HT} € HT</p>

          {availability.now ? (
            <p className="desk-availability">
              Disponible dès maintenant
            </p>
          ) : (
            <p className="desk-availability">
              Disponible à partir du{" "}
              {availability.day}{" "}
              {availability.month}
            </p>
          )}

          <Link
            href="/contact"
            onClick={() =>
              handleRedirect(
                `Réservation du Bureau ${title}`,
              )
            }
            className="btn-header"
          >
            Réserver
          </Link>
        </div>
        <p className="desk-mb">{description}</p>
        <p className="desk-mb">{text}</p>
        <p>
          Il est disponible à un tarif mensuel
          très raisonnable permettant d'avoir
          votre entreprise basé directement au
          coeur de Lille juste à côté des gares
          Lille Europe et Lille Flandres. Si vous
          êtes intéressé par ce bureau, n'hésitez
          pas à nous contacter pour plus de
          détails et pour organiser une visite.
        </p>
      </div>
      <span className="desk-separator"></span>
    </div>
  );
};

export default Desk;
