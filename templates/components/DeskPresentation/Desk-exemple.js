import React from "react";
import Slider from "react-slick";

const Desk = ({ id, img, title, text }) => {
  return (
    <div className="desk-card" id={id}>
      <div className="desk-card-img-holder">
        <img
          className="desk-card-img"
          src={img}
          alt="Bureau disponible en co working venant des locaux de WorkUnion"
        />
      </div>
      <div className="desk-card-title">
        <h2>{title}</h2>
      </div>
      <div className="desk-card-anim desk-anim1">
        <div className="desk-card-text">
          {text}
        </div>
        <div className="desk-card-anim desk-anim2">
          <div className="desk-card-btn">
            <a
              href="#"
              className="btn-contact desk-btn"
            >
              Réservation
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Desk;
