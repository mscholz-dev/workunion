import React from "react";

const IconSocial = ({ url, icon }) => {
  return (
    <a
      href={url}
      className="icon-social-container"
      target="blank"
    >
      <span className="icon-social">{icon}</span>
    </a>
  );
};

export default IconSocial;
