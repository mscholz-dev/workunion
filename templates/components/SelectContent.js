import React, { useState } from "react";
import Section from "../layouts/Section";
import Container from "../layouts/Container";

const SelectContent = ({ title, items }) => {
  const [currentItem, setCurrentItem] = useState({
    title: items[0].title || "",
    content: items[0].content || "",
  });

  const handleItem = (item, id) => {
    if (id === item.id) return item;
    return false;
  };

  const handleClick = (id) => {
    const item = items.find((item) =>
      handleItem(item, id),
    );

    setCurrentItem({
      title: item.title || "",
      content: item.content || "",
    });
  };

  return (
    <Section>
      <div className="select-content">
        <h2 className="select-content-title">
          {title}
        </h2>

        <div className="select-content-wrapper">
          <div className="select-content-select">
            {items.map(({ id, title, price }) => (
              <button
                key={id}
                className={`select-content-select-btn${
                  currentItem.title === title
                    ? " select-content-select-btn-active"
                    : ""
                }`}
                onClick={() => handleClick(id)}
              >
                <h3 className="select-content-select-title">
                  {title}
                </h3>
                <p className="select-content-select-price">
                  {price}€ HT/mois
                </p>
              </button>
            ))}
          </div>

          <div className="select-content-content">
            <h4 className="select-content-content-title">
              {currentItem.title}
            </h4>
            <div
              dangerouslySetInnerHTML={{
                __html: currentItem.content,
              }}
            />
          </div>
        </div>
      </div>
    </Section>
  );
};

export default SelectContent;
