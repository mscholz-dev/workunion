import React from "react";

const Home = () => {
  return (
    <div className="home-container">
      <video
        alt="Gare Lille Flandres, Grand Place de Lille"
        src="/video/back.mp4"
        autoPlay
        loop
        muted
        className="home-video"
        id="accueil"
      />
      <h1 className="home-title">
        <span className="home-title-brand">
          WORKUNION
        </span>{" "}
        <span className="home-title-content">
          DOMICILIATION - BUREAUX À DISPOSITION
        </span>
      </h1>
      <div className="home-contact">
        <span className="home-contact-content">
          07 83 200 600
        </span>
        <span className="home-contact-content">
          <a
            href="mailto:contact.workunion@gmail.com"
            className="text-secondaire text-bold"
          >
            contact.workunion@gmail.com
          </a>
        </span>
        <span className="home-contact-content">
          <a href="https://www.google.com/maps/place/16+Rue+Faidherbe,+59800+Lille/@50.6368875,3.0639326,17z/data=!3m1!4b1!4m5!3m4!1s0x47c2d589ab427ce3:0x4e57745c5f7bf46a!8m2!3d50.6368841!4d3.0661213?hl=fr">
            16-18 Rue Faidherbe, Lille 59000
          </a>
        </span>
      </div>
    </div>
  );
};

export default Home;
