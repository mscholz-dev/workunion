import React, {
  useState,
  useRef,
  useEffect,
} from "react";
import IconChevron from "../../public/icons/chevron.svg";

const FaqItem = ({ title, content }) => {
  const mainRef = useRef(null);
  const contentRef = useRef(null);

  const [open, setOpen] = useState(false);

  const handleClick = () => {
    if (!contentRef.current || !mainRef.current)
      return;

    if (open) {
      contentRef.current.style.height = "0px";
      setOpen(false);
      return;
    }

    setOpen(true);

    let contentHeight = 0;
    for (const item of contentRef.current
      .children) {
      contentHeight += item.offsetHeight + 12;
    }

    contentRef.current.style.height = `${
      contentHeight + 12
    }px`;
  };

  const handleResize = () => {
    if (
      !contentRef.current ||
      contentRef.current.style.height === "0px" ||
      !contentRef.current.style.height
    )
      return;

    let contentHeight = 0;
    for (const item of contentRef.current
      .children) {
      contentHeight += item.offsetHeight + 12;
    }

    contentRef.current.style.height = `${
      contentHeight + 12
    }px`;
  };

  useEffect(() => {
    window.addEventListener(
      "resize",
      handleResize,
    );
  }, []);

  return (
    <div
      ref={mainRef}
      className={`faq-item${
        open ? " faq-item-open" : ""
      }`}
    >
      <div className="faq-item-header">
        <h2 className="faq-item-title">
          {title}
        </h2>
        <button
          className="faq-item-icon"
          onClick={handleClick}
        >
          <IconChevron />
        </button>
      </div>

      <span
        ref={contentRef}
        className="faq-item-content"
        dangerouslySetInnerHTML={{
          __html: content,
        }}
      />
    </div>
  );
};

export default FaqItem;
